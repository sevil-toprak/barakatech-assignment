package com.barakatech.assignment.controller;

import com.barakatech.assignment.controller.request.BuyAndSellCryptoRequest;
import com.barakatech.assignment.controller.response.BalanceResponse;
import com.barakatech.assignment.controller.response.MessageResponse;
import com.barakatech.assignment.service.UserCryptoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/crypto")
public class CryptoCurrencyController {

    private static final Logger logger = LoggerFactory.getLogger(CryptoCurrencyController.class);

    UserCryptoService userCryptoService;

    public CryptoCurrencyController(UserCryptoService userCryptoService) {
        this.userCryptoService = userCryptoService;
    }

    @GetMapping("/getBalance")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<?> getBalance(@RequestParam String username) {

        BigDecimal balance = userCryptoService.getBalance(username);

        return ResponseEntity.ok(new BalanceResponse(username,
                balance,
                "USDT"));
    }

    @GetMapping("/getRemainingUSDT")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<?> getRemainingUSDT(@RequestParam String username) {

        BigDecimal balance = userCryptoService.getRemainingUSDT(username);

        return ResponseEntity.ok(new BalanceResponse(username,
                balance,
                "USDT"));
    }

    @PostMapping("/buy")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<?> buyCrypto(@RequestBody BuyAndSellCryptoRequest request) {

        Boolean result = userCryptoService.buyCrypto(request);

        String message = "Crypto has been successfully purchased.";
        if(!result)
            message = "Purchase of coins failed.";

        return ResponseEntity
                .badRequest()
                .body(new MessageResponse(message));
    }

    @PostMapping("/sell")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<?> sellCrypto(@RequestBody BuyAndSellCryptoRequest request) {

        Boolean result = userCryptoService.sellCrypto(request);

        String message = "Crypto has been successfully sold.";
        if(!result)
            message = "Sale of coins failed.";

        return ResponseEntity
                .badRequest()
                .body(new MessageResponse(message));
    }

}
