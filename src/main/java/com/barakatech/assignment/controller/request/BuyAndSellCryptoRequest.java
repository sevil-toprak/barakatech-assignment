package com.barakatech.assignment.controller.request;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

public class BuyAndSellCryptoRequest {

        @NotBlank
        private String username;

        @NotBlank
        private String type;

        @NotBlank
        private BigDecimal amount;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public BigDecimal getAmount() {
            return amount;
        }

        public void setAmount(BigDecimal amount) {
            this.amount = amount;
        }
}
