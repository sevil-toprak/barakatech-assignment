package com.barakatech.assignment.controller.response;

import java.math.BigDecimal;

public class BalanceResponse {

    private String username;
    private BigDecimal amount;
    private String type;

    public BalanceResponse() {
    }

    public BalanceResponse(String username, BigDecimal amount, String type) {
        this.username = username;
        this.amount = amount;
        this.type = type;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "BalanceResponse{" +
                "username='" + username + '\'' +
                ", amount=" + amount +
                ", type='" + type + '\'' +
                '}';
    }
}
