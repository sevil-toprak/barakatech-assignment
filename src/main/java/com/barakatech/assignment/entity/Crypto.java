package com.barakatech.assignment.entity;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@Table(name = "crypto")
public class Crypto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = 20)
    private String name;

    @NotBlank
    @Size(max = 50)
    private String code;

    @OneToMany(mappedBy = "crypto")
    Set<UserCrypto> userCryptoSet;

    public Crypto() {
    }

    public Crypto(Long id, @NotBlank @Size(max = 20) String name, @NotBlank @Size(max = 50) String code, Set<UserCrypto> userCryptoSet) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.userCryptoSet = userCryptoSet;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Set<UserCrypto> getUserCryptoSet() {
        return userCryptoSet;
    }

    public void setUserCryptoSet(Set<UserCrypto> userCryptoSet) {
        this.userCryptoSet = userCryptoSet;
    }
}
