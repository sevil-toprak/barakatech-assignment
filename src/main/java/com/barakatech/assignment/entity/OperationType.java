package com.barakatech.assignment.entity;

public enum OperationType {
    BUY,
    SELL
}