package com.barakatech.assignment.entity;

public enum Roles {
    ROLE_USER,
    ROLE_ADMIN
}