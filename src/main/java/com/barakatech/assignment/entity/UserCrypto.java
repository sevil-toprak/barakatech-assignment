package com.barakatech.assignment.entity;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "user_crypto")
public class UserCrypto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    User user;

    @ManyToOne
    @JoinColumn(name = "crypto_id")
    Crypto crypto;

    BigDecimal amount;

    public UserCrypto() {
    }

    public UserCrypto(Long id, User user, Crypto crypto, BigDecimal amount) {
        this.id = id;
        this.user = user;
        this.crypto = crypto;
        this.amount = amount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Crypto getCrypto() {
        return crypto;
    }

    public void setCrypto(Crypto crypto) {
        this.crypto = crypto;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
