package com.barakatech.assignment.entity;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "transaction_history")
public class TransactionHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = 20)
    private String username;

    @Enumerated(EnumType.STRING)
    private OperationType operation;

    @NotBlank
    private BigDecimal amount;

    @NotBlank
    @Size(max = 20)
    private String cryptoType;

    @Type(type="date")
    private Date time;

    public TransactionHistory() {
    }

    public TransactionHistory(@NotBlank @Size(max = 20) String username, OperationType operation, @NotBlank BigDecimal amount, @NotBlank @Size(max = 20) String cryptoType, Date time) {
        this.username = username;
        this.operation = operation;
        this.amount = amount;
        this.cryptoType = cryptoType;
        this.time = time;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public OperationType getOperation() {
        return operation;
    }

    public void setOperation(OperationType operation) {
        this.operation = operation;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCryptoType() {
        return cryptoType;
    }

    public void setCryptoType(String cryptoType) {
        this.cryptoType = cryptoType;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}
