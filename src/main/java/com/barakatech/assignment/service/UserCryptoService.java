package com.barakatech.assignment.service;

import com.barakatech.assignment.controller.request.BuyAndSellCryptoRequest;
import com.barakatech.assignment.entity.Crypto;
import com.barakatech.assignment.entity.OperationType;
import com.barakatech.assignment.entity.User;
import com.barakatech.assignment.entity.UserCrypto;
import com.barakatech.assignment.repository.CryptoRepository;
import com.barakatech.assignment.repository.UserCryptoRepository;
import com.barakatech.assignment.repository.UserRepository;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

@Service
@Transactional
public class UserCryptoService {

    private static final Logger logger = LoggerFactory.getLogger(UserCryptoService.class);

    private final UserCryptoRepository userCryptoRepository;

    private final UserRepository userRepository;

    private final CryptoRepository cryptoRepository;

    private final TransactionHistoryService transactionHistoryService;

    public UserCryptoService(UserCryptoRepository userCryptoRepository, UserRepository userRepository, CryptoRepository cryptoRepository,
                             TransactionHistoryService transactionHistoryService) {
        this.userCryptoRepository = userCryptoRepository;
        this.userRepository = userRepository;
        this.cryptoRepository = cryptoRepository;
        this.transactionHistoryService = transactionHistoryService;
    }

    public BigDecimal getBalance(String username) {
        logger.debug("getBalance with usernaem: {}", username);

        BigDecimal amount = new BigDecimal(0);

        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));

        Set<UserCrypto> userCryptoList = userCryptoRepository.findByUserId(user.getId());

        HashMap<String, BigDecimal> prices = getLivePrices();

        for (Iterator<UserCrypto> iter = userCryptoList.iterator(); iter.hasNext(); ) {
            UserCrypto userCrypto = iter.next();
            String crypto_code = userCrypto.getCrypto().getCode().toUpperCase();
            BigDecimal cryptoPrice = crypto_code.equals("USDT") ? new BigDecimal(1) : prices.get(crypto_code);
            amount = amount.add(userCrypto.getAmount().multiply(cryptoPrice));
        }

        return amount;
    }

    public BigDecimal getRemainingUSDT(String username) {
        logger.debug("getRemainingUSDT with usernaem: {}", username);

        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));

        Crypto crypto = cryptoRepository.findByCode("USDT").orElseThrow(() -> new UsernameNotFoundException("Crypto Not Found with code: USDT"));
        UserCrypto userCrypto = userCryptoRepository.findByUserIdAndCryptoId(user.getId(), crypto.getId());

        return userCrypto.getAmount();
    }

    public Boolean buyCrypto(BuyAndSellCryptoRequest request) {
        logger.debug("buyCrypto with usernaem: {}", request.getUsername());
        boolean result = true;

        User user = userRepository.findByUsername(request.getUsername())
                .orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + request.getUsername()));

        Crypto cryptoUSDT = cryptoRepository.findByCode("USDT").orElseThrow(() -> new UsernameNotFoundException("Crypto Not Found with code: USDT"));
        UserCrypto userCryptoUSDT = userCryptoRepository.findByUserIdAndCryptoId(user.getId(), cryptoUSDT.getId());

        HashMap<String, BigDecimal> prices = getLivePrices();

        BigDecimal purchasedAmount = prices.get(request.getType()).multiply(request.getAmount());

        if(purchasedAmount.compareTo(userCryptoUSDT.getAmount()) == 1) {
            result = false;
        } else {
            userCryptoUSDT.setAmount(userCryptoUSDT.getAmount().subtract(purchasedAmount));

            Crypto purchasedCrypto = cryptoRepository.findByCode(request.getType()).orElseThrow(() -> new UsernameNotFoundException("Crypto Not Found with code: " + request.getType()));
            UserCrypto purchasedUserCrypto = (UserCrypto) userCryptoRepository.findByUserIdAndCryptoId(user.getId(), purchasedCrypto.getId());
            purchasedUserCrypto.setAmount(purchasedUserCrypto.getAmount().add(request.getAmount()));

            transactionHistoryService.addHistory(request.getUsername(), OperationType.BUY, request.getAmount(), request.getType());
        }

        return result;
    }

    public Boolean sellCrypto(BuyAndSellCryptoRequest request) {
        logger.debug("sellCrypto with usernaem: {}", request.getUsername());
        boolean result = true;

        User user = userRepository.findByUsername(request.getUsername())
                .orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + request.getUsername()));

        Crypto soldCrypto = cryptoRepository.findByCode(request.getType()).orElseThrow(() -> new UsernameNotFoundException("Crypto Not Found with code: " + request.getType()));

        UserCrypto soldUserCrypto = userCryptoRepository.findByUserIdAndCryptoId(user.getId(), soldCrypto.getId());

        HashMap<String, BigDecimal> prices = getLivePrices();

        BigDecimal soldAmount = prices.get(request.getType()).multiply(request.getAmount());

        if(soldUserCrypto.getAmount().compareTo(request.getAmount()) == -1) {
            result = false;
        } else {
            soldUserCrypto.setAmount(soldUserCrypto.getAmount().subtract(request.getAmount()));

            Crypto cryptoUSDT = cryptoRepository.findByCode("USDT").orElseThrow(() -> new UsernameNotFoundException("Crypto Not Found with code: USDT"));
            UserCrypto userCryptoUSDT = (UserCrypto) userCryptoRepository.findByUserIdAndCryptoId(user.getId(), cryptoUSDT.getId());
            userCryptoUSDT.setAmount(userCryptoUSDT.getAmount().add(soldAmount));

            transactionHistoryService.addHistory(request.getUsername(), OperationType.SELL, request.getAmount(), request.getType());
        }

        return result;
    }

    private HashMap<String, BigDecimal> getLivePrices() {
        final String uri = "https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&ids=bitcoin,ethereum";
        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(uri, String.class);

        JsonParser jsonParser = new JsonParser();
        JsonArray jsonArray = (JsonArray) jsonParser.parse(result);

        HashMap<String, BigDecimal> prices = new HashMap<String, BigDecimal>();

        for (Iterator<JsonElement> iter = jsonArray.iterator(); iter.hasNext(); ) {
            JsonElement element = iter.next();
            String symbol = ((JsonObject) element).get("symbol").getAsString().toUpperCase();
            BigDecimal price = ((JsonObject) element).get("current_price").getAsBigDecimal();
            prices.put(symbol, price);
        }

        return prices;
    }
}
