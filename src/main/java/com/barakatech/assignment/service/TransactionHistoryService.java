package com.barakatech.assignment.service;

import com.barakatech.assignment.entity.OperationType;
import com.barakatech.assignment.entity.TransactionHistory;
import com.barakatech.assignment.repository.TransactionHistoryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;

@Service
@Transactional
public class TransactionHistoryService {

    private static final Logger logger = LoggerFactory.getLogger(TransactionHistoryService.class);

    public static final int PAGE_NUMBER = 0;
    public static final int PAGE_SIZE = 20;

    private final TransactionHistoryRepository transactionHistoryRepository;

    public TransactionHistoryService(TransactionHistoryRepository transactionHistoryRepository) {
        this.transactionHistoryRepository = transactionHistoryRepository;
    }

    public void addHistory(String username, OperationType operationType, BigDecimal amount, String cryptoType) {
        TransactionHistory transactionHistory = new TransactionHistory(username, operationType, amount, cryptoType, new Date());
        transactionHistoryRepository.save(transactionHistory);
    }

    public Page<TransactionHistory> getHistory(String username, Pageable page) {

        logger.debug("Get history with username: {}", username);

        if (page == null)
            page = PageRequest.of(PAGE_NUMBER, PAGE_SIZE);

        return transactionHistoryRepository.findByUsername(username, page);
    }

}
