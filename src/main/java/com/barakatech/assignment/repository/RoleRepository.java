package com.barakatech.assignment.repository;

import java.util.Optional;

import com.barakatech.assignment.entity.Roles;
import com.barakatech.assignment.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(Roles name);
}
