package com.barakatech.assignment.repository;

import com.barakatech.assignment.entity.UserCrypto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface UserCryptoRepository extends JpaRepository<UserCrypto, Long> {

    Set<UserCrypto> findByUserId(Long userId);

    UserCrypto findByUserIdAndCryptoId(Long userId, Long cryptoId);
}
