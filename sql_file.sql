-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               PostgreSQL 13.3, compiled by Visual C++ build 1914, 64-bit
-- Server OS:                    
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES  */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping data for table public.crypto: 0 rows
DELETE FROM "crypto";
/*!40000 ALTER TABLE "crypto" DISABLE KEYS */;
INSERT INTO "crypto" ("id", "code", "name") VALUES
	(1, 'USDT', 'Tether'),
	(3, 'BTC', 'Bitcoin'),
	(6, 'ETH', 'Ethereum');
/*!40000 ALTER TABLE "crypto" ENABLE KEYS */;

-- Dumping data for table public.role: 0 rows
DELETE FROM "role";
/*!40000 ALTER TABLE "role" DISABLE KEYS */;
INSERT INTO "role" ("id", "name") VALUES
	(1, 'ROLE_USER'),
	(3, 'ROLE_ADMIN');
/*!40000 ALTER TABLE "role" ENABLE KEYS */;

-- Dumping data for table public.transaction_history: 0 rows
DELETE FROM "transaction_history";
/*!40000 ALTER TABLE "transaction_history" DISABLE KEYS */;
INSERT INTO "transaction_history" ("id", "amount", "crypto_type", "operation", "time", "username") VALUES
	(1, 1.00, 'ETH', 'BUY', '2021-05-23', 'username'),
	(2, 2.00, 'ETH', 'SELL', '2021-05-23', 'username'),
	(3, 1.00, 'ETH', 'BUY', '2021-05-23', 'username'),
	(4, 1.00, 'BTC', 'BUY', '2021-05-23', 'test2');
/*!40000 ALTER TABLE "transaction_history" ENABLE KEYS */;

-- Dumping data for table public.users: 0 rows
DELETE FROM "users";
/*!40000 ALTER TABLE "users" DISABLE KEYS */;
INSERT INTO "users" ("id", "email", "password", "username") VALUES
	(1, 'test@test.com', '$2a$04$xeydzMKUZk1lWZhgZ21Ed.RcUQjjagcnyvLBc9Kz3JFFbzbvVyIBy', 'username'),
	(3, 'test@test2.com', '$2a$04$xeydzMKUZk1lWZhgZ21Ed.RcUQjjagcnyvLBc9Kz3JFFbzbvVyIBy', 'test2');
/*!40000 ALTER TABLE "users" ENABLE KEYS */;

-- Dumping data for table public.user_crypto: 0 rows
DELETE FROM "user_crypto";
/*!40000 ALTER TABLE "user_crypto" DISABLE KEYS */;
INSERT INTO "user_crypto" ("id", "amount", "crypto_id", "user_id") VALUES
	(2, 20.00, 3, 1),
	(4, 40.00, 3, 3),
	(1, 507063.11, 1, 1),
	(3, 29.00, 6, 1);
/*!40000 ALTER TABLE "user_crypto" ENABLE KEYS */;

-- Dumping data for table public.user_role: 0 rows
DELETE FROM "user_role";
/*!40000 ALTER TABLE "user_role" DISABLE KEYS */;
INSERT INTO "user_role" ("user_id", "role_id") VALUES
	(1, 3),
	(3, 1),
	(1, 1),
	(3, 3);
/*!40000 ALTER TABLE "user_role" ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
