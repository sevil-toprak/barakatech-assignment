Postman collection'ı ve sql dump'ı da paylaşıyorum. Dosya boyutları büyük olmadığı için repo'ya yükledim.

Docker image'i DockerHub'a yükleyemedim. Sadece yml dosyasını paylaşıyorum.

Kaynaklar:
JWT yapısında aşağıda verdiğim kaynaklardan faydalandım.
    - https://bezkoder.com/spring-boot-jwt-authentication/
    - jhipster JWT entegrasyonu. https://github.com/jhipster/jhipster-sample-app
